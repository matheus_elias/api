FactoryBot.define do
  factory :feedback do
    rate 7
    pending false
    evaluated_user user
    user_id user
  end
end