class Api::V1::FeedbackSerializer < ActiveModel::Serializer
  attributes :rate, :pending, :evaluated_user, :user_id, :evaluated_user_name, :evaluer

  def evaluated_user_name
    User.find(object.evaluated_user).name
  end

  def evaluer
    User.find(object.user_id).name
  end
end
