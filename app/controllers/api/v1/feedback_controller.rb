module Api
  module V1
    class FeedbackController < Api::V1::ApiController
      before_action :authenticate_user, only: %i[update destroy create show]
      before_action :set_feedback, only: %i[show destroy update]

      def index
        feedback = Feedback.all
        render json: feedback
      end

      def create
        feedback = Feedback.new(feedback_params)
        if feedback.save
          render json: feedback, status: :created
        else
          render json: { errors: feedback.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def update
        if @feedback.update(feedback_params)
          render json: @feedback
        else
          render json: { errors: @feedback.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def destroy
        @feedback.destroy
      end

      
      def show
        render json: @feedback
      end

      private

      def feedback_params
        params.require(:feedback).permit(:rate, :pending, :evaluated_user, :user_id)
      end

      def set_feedback
        @feedback = feedback.find(params[:id])
      end
    end
  end
end