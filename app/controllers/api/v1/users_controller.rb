module Api
  module V1
    class UsersController < Api::V1::ApiController
      before_action :authenticate_user, only: %i[index current update destroy create show feedback]
      before_action :set_user, only: %i[show destroy update]

      def index
        users = User.all
        render json: users
      end

      def create
        user = User.new(user_params)
        if user.save
          render json: user, status: :created
        else
          render json: { errors: user.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def update
        if @user.update(user_params)
          render json: @user
        else
          render json: { errors: @user.errors.full_messages }, status: :unprocessable_entity
        end
      end

      def destroy
        @user.destroy
      end

      
      def show
        render json: @user
      end
      
      def current
        render json: current_user
      end

      def feedback
        render json: current_user.feedback
      end

      private

      def user_params
        params.require(:user).permit(:name, :email, :responsibility, :password, :password_confirmation)
      end

      def set_user
        @user = User.find(params[:id])
      end
    end
  end
end