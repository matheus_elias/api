Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post 'user_token', to: 'user_token#create'

      resources :feedback
      resources :users, only: %i[index show create update destroy] do
        get 'feedback', on: :collection
        get 'current', on: :collection
      end
    end
  end
end